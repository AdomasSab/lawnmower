from readers import FileReader
from handlers import LawnMowerHandler
from mowers import LawnMower


class PositionCalculator():

    def printLawnMowerPosition(self, limits, lawn_mower_data):
        for data in lawn_mower_data:
            lawn_mower = LawnMower(position=data.position,
                                   direction=data.direction, limits=limits)
            handler = LawnMowerHandler()
            for cmd in data.command:
                handler.handle(command=cmd, lawn_mower=lawn_mower)
            lawn_mower.printPosition()


if __name__ == "__main__":

    limits, lawn_mower_data = FileReader().getData('../myfile.txt')
    PositionCalculator().printLawnMowerPosition(limits, lawn_mower_data)
