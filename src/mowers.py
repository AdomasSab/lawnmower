DIRECTIONS = ['N', 'E', 'S', 'W']


class LawnMower:

    def __init__(self, position, direction, limits):
        self.position = position
        self.direction = direction
        self.limits = limits

    def printPosition(self):
        print(self.position[0] ,self.position[1], self.direction)

    def rotate(self, direction):
        index = DIRECTIONS.index(self.direction)
        if (direction == 'left'):
            if (index > 0):
                return DIRECTIONS[index - 1]
            else:
                return DIRECTIONS[len(DIRECTIONS) - 1]

        if (direction == 'right'):
            if (index == len(DIRECTIONS) - 1):
                return DIRECTIONS[0]
            else:
                return DIRECTIONS[index + 1]

    def turnLeft(self):
        self.direction = self.rotate('left')

    def turnRight(self):
        self.direction = self.rotate('right')

    def validateMove(self, x, y):
        return (x >= 0 and x <= self.limits[0]) and (y >= 0 and y <= self.limits[1])

    def move(self):
        if (self.direction == 'N' and self.validateMove(self.position[0], self.position[1] + 1)):
            self.position[1] += 1
            return

        if (self.direction == 'E' and self.validateMove(self.position[0] + 1, self.position[1])):
            self.position[0] += 1
            return

        if (self.direction == 'S' and self.validateMove(self.position[0], self.position[1] - 1)):
            self.position[1] -= 1
            return

        if (self.direction == 'W' and self.validateMove(self.position[0] - 1, self.position[1])):
            self.position[0] -= 1
            return
