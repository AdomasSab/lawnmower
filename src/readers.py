from typing import NamedTuple


class Data(NamedTuple):
    position: list
    direction: str
    command: str


class FileReader:

    def getData(self, fileName):
        with open(fileName) as f:
            lines = f.readlines()
            x, y = lines[0].split()
            limit = [int(x), int(y.strip())]

            i = 1
            control_data = list()
            while i < len(lines):
                x, y, direction = lines[i].strip().split(" ", 2)
                command = lines[i+1].strip()
                control_data.append(Data([int(x), int(y)], direction, command))
                i += 2

        return limit, control_data
