class LawnMowerHandler:

    def handle(self, command, lawn_mower):
        if command == 'L':
            lawn_mower.turnLeft()
            return
        if command == 'R':
            lawn_mower.turnRight()
            return
        if (command == 'F'):
            lawn_mower.move()
            return
        return
