import sys
import os
import io
sys.path.insert(0, os.path.dirname(
    os.path.dirname(os.path.realpath(__file__))))
from src.mowers import LawnMower
import unittest
from unittest.mock import patch


class TestLawnMower(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.lawnMower = LawnMower([0,0], 'N', [0,0])

    def tearDown(self):
        self.lawnMower = None

    def test_initialize_lawn_mower(self):
        print(self._testMethodName)  

        self.assertEqual(self.lawnMower.position, [0, 0], "Should be 0 0")
        self.assertEqual(self.lawnMower.direction, 'N', "should be N")
        self.assertEqual(self.lawnMower.limits, [0,0], 'Should be [0,0]')

    def test_rotate(self):
        print(self._testMethodName)

        rotationDirection =  self.lawnMower.rotate('left')
        self.assertEqual(rotationDirection, 'W', "should be W")
        rotationDirection = self.lawnMower.rotate('right')
        self.assertEqual(rotationDirection, 'E', "should be W")
        rotationDirection = self.lawnMower.rotate('falseInput')
        self.assertEqual(rotationDirection, None, "should be None")

    def test_turnLeft(self):
        print(self._testMethodName)
        
        self.lawnMower.turnLeft()
        self.assertEqual(self.lawnMower.direction, 'W', "should be W")
        self.lawnMower.turnLeft()
        self.assertEqual(self.lawnMower.direction, 'S', "should be S")
        self.lawnMower.turnLeft()
        self.assertEqual(self.lawnMower.direction, 'E', "should be E")
        self.lawnMower.turnLeft()
        self.assertEqual(self.lawnMower.direction, 'N', "should be N")

    def test_turnRight(self):
        print(self._testMethodName)
        
        self.lawnMower.turnRight()
        self.assertEqual(self.lawnMower.direction, 'E', "should be W")
        self.lawnMower.turnRight()
        self.assertEqual(self.lawnMower.direction, 'S', "should be S")
        self.lawnMower.turnRight()
        self.assertEqual(self.lawnMower.direction, 'W', "should be E")
        self.lawnMower.turnRight()
        self.assertEqual(self.lawnMower.direction, 'N', "should be N")

    def test_validateMove(self):
        print(self._testMethodName)

        self.assertEqual(self.lawnMower.validateMove(1,0), False, "should be False")
        self.lawnMower.limits = [1,0]
        self.assertEqual(self.lawnMower.validateMove(1,0), True, "should be True")

    def test_move(self):
        print(self._testMethodName) 

        self.lawnMower.move()
        self.assertEqual(self.lawnMower.position, [0,0], 'Should not move')

        self.lawnMower.limits = [0,1]
        self.lawnMower.move()

        self.assertEqual(self.lawnMower.position, [0,1], 'Should move')

    def test_printPosition(self):
        print(self._testMethodName)
        
        self.lawnMower.position = [1,1]
        self.lawnMower.direction = 'W'

        with patch('sys.stdout', new = io.StringIO()) as mocked_output: 
            self.lawnMower.printPosition()
            self.assertEqual(mocked_output.getvalue(),  '1 1 W\n' ) 



if __name__ == '__main__':
    unittest.main()
