import sys
import os
import io
sys.path.insert(0, os.path.dirname(
    os.path.dirname(os.path.realpath(__file__))))
from src.readers import FileReader
import unittest
from unittest.mock import patch



class TestFileReader(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.fileReader = FileReader()

    def tearDown(self):
        self.fileReader = None

    def test_getData(self):
        print(self._testMethodName)
        
        limits, data = self.fileReader.getData('../test/data/baseFile_test.txt')
        self.assertEqual(limits, [5,5], 'Should be [5,5]')
        self.assertEqual(len(data), 1, 'Should be 1')
        self.assertEqual(data[0].position, [1,2])
        self.assertEqual(data[0].direction, 'N')
        self.assertEqual(data[0].command, 'LFLFLFLFF')

    def test_fileNotFoundError(self):
        print(self._testMethodName)

        with self.assertRaises(FileNotFoundError): self.fileReader.getData('')

    def test_valueError(self):
        print(self._testMethodName)

        with self.assertRaises(ValueError): self.fileReader.getData('../test/data/valueError_test.txt')
        



if __name__ == '__main__':
    unittest.main()
