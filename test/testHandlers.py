import sys
import os
import io
sys.path.insert(0, os.path.dirname(
    os.path.dirname(os.path.realpath(__file__))))
from src.mowers import LawnMower
from src.handlers import LawnMowerHandler
import unittest
from unittest.mock import patch



class TestLawnMower(unittest.TestCase):

    @classmethod
    def setUp(self):
        
        self.lawnMower = self.lawnMower = LawnMower([0, 0], 'N', [0, 0])
        self.lawnMowerHandler = LawnMowerHandler()
        self.mockLawnMowerMove = patch.object(LawnMower, 'move')
        self.mockLawnMowerTurnLeft = patch.object(LawnMower, 'turnLeft')
        self.mockLawnMowerTurnRight = patch.object(LawnMower, 'turnRight')

    def tearDown(self):
        self.lawnMowerHandler = None
        self.lawnMower = None

    def test_handle(self):
        print(self._testMethodName)

        with self.mockLawnMowerMove as mockMove:
            self.lawnMowerHandler.handle('F', self.lawnMower)
        self.assertTrue(mockMove.called)

        with self.mockLawnMowerTurnLeft as mockTurnLeft:
            self.lawnMowerHandler.handle('L', self.lawnMower)
        self.assertTrue(mockTurnLeft.called)

        with self.mockLawnMowerTurnRight as mockTurnRight:
            self.lawnMowerHandler.handle('R', self.lawnMower)
        self.assertTrue(mockTurnRight.called)

    def test_handle_unsupportedCommand(self):
        print(self._testMethodName)

        with self.mockLawnMowerMove as mockMove:
            self.lawnMowerHandler.handle('notSupported', self.lawnMower)
        self.assertFalse(mockMove.called)

        with self.mockLawnMowerTurnLeft as mockTurnLeft:
            self.lawnMowerHandler.handle('notSupported', self.lawnMower)
        self.assertFalse(mockTurnLeft.called)

        with self.mockLawnMowerTurnRight as mockTurnRight:
            self.lawnMowerHandler.handle('notSupported', self.lawnMower)
        self.assertFalse(mockTurnRight.called)


if __name__ == '__main__':
    unittest.main()
